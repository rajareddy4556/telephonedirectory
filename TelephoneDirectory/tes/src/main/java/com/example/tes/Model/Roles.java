package com.example.tes.Model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Roles {
    @Id
    private int rolenum;
    @Column
    private String rolename;

    public int getRolenum() {
        return rolenum;
    }

    public void setRolenum(int rolenum) {
        this.rolenum = rolenum;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
}
