package com.example.tes.Model;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity // This tells Hibernate to make a table out of this class
public class Users {
    @Id
    private String username;
    //@GeneratedValue(strategy=GenerationType.AUTO)
    private Integer userid;

    private String password;
    @ManyToMany
    @JoinTable(name = "telephonedirectory",joinColumns = @JoinColumn(name = "username"),
            inverseJoinColumns = @JoinColumn(name = "contactnum"))
    private  List<Contacts> contacts=new ArrayList<>();



    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

