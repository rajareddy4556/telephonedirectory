package com.example.tes.Controller;
import com.example.tes.Repositorys.ContactsRepository;
import com.example.tes.Model.Contacts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/hey")
public class ContactsController {

    @Autowired
    private ContactsRepository contactsRepository;
    @PostMapping(path="/add") // Map ONLY POST Requests
    public @ResponseBody String addNewUser (@RequestParam long contactnum
            , @RequestParam String contactname) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        Contacts contacts=new Contacts();
        contacts.setContactnum(contactnum);
        contacts.setContactname(contactname);
        contactsRepository.save(contacts);
        return "Saved";
    }
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Contacts> getAllUsers() {
        // This returns a JSON or XML with the users
        return contactsRepository.findAll();
    }
}
