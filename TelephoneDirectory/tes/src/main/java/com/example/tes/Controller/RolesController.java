package com.example.tes.Controller;

import com.example.tes.Model.Roles;
import com.example.tes.Repositorys.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/hel")
public class RolesController {
   @Autowired
    private RolesRepository rolesRepository;
    @PostMapping(path="/add") // Map ONLY POST Requests
    public @ResponseBody String addNewUser (@RequestParam int rolenum
            , @RequestParam String rolename) {
        // @ResponseBody means the returned String is the response, not a view name
        // @RequestParam means it is a parameter from the GET or POST request

        Roles roles=new Roles();
        roles.setRolenum(rolenum);
        roles.setRolename(rolename);
        rolesRepository.save(roles);
        return "Saved";
    }
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Roles> getAllUsers() {
        // This returns a JSON or XML with the users
        return rolesRepository.findAll();
    }


}
