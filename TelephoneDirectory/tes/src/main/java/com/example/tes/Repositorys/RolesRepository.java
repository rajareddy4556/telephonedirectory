package com.example.tes.Repositorys;

import com.example.tes.Model.Roles;
import org.springframework.data.repository.CrudRepository;

public interface RolesRepository extends CrudRepository<Roles,Integer> {

}
