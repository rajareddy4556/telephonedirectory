package com.example.tes.Model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Contacts {
    @Id
    private long contactnum;
    @Column
    private String contactname;
    @ManyToMany(mappedBy = "contacts")
    private List<Users> users=new ArrayList<>();


    public long getContactnum() {
        return contactnum;
    }

    public void setContactnum(long contactnum) {
        this.contactnum = contactnum;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }
}
