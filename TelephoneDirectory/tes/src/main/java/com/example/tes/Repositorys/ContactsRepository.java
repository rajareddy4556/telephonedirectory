package com.example.tes.Repositorys;

import com.example.tes.Model.Contacts;
import org.springframework.data.repository.CrudRepository;

public interface ContactsRepository extends CrudRepository<Contacts,Long> {
}
